export const goods = [
  {
    name: 'Good 1',
    imageSource: './public/images/1.jpg',
    shortDescription: 'shor desc for 1 good',
    fullDescription: `it's so long long story about this good... it's began when i try coding in java, thinking in java, fcked in java`,
    features: [{feature: 'strong', count: 50}, {feature: 'spam', count: 10}]
  },
  {
    name: 'Good 11',
    imageSource: '../../public/images/2.jpg',
    shortDescription: 'shor desc for 2 good',
    fullDescription: `it's so long long story about this good... it's began when i try coding in java, thinking in java, fcked in java`,
    features: [{feature: 'strong', count: 250}, {feature: 'spam', count: 110}]
  }
]
