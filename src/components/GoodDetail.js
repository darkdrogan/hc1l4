import React from 'react'

const GoodDetail = () =>
  <div>
    <h1> Товар 1</h1>

    <a href="../images/1.jpg" target="_blank">
      <img className="full-description" style="width: 150px; height: 200px;" src="../images/1.jpg" alt="Товар 1"
           title="Товар 1"/>
    </a>
    <p className="full-description">
      full description
    </p>
    <ul className="features">
      <li>
        <strong>Характеристика 1:</strong> показатель
      </li>
      <li>
        <strong>Характеристика 1:</strong> показатель
      </li>
      <li>
        <strong>Характеристика 1:</strong> показатель
      </li>
      <li>
        <strong>Характеристика 1:</strong> показатель
      </li>
    </ul>
  </div>

export default GoodDetail
