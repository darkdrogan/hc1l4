import React from 'react'
import HeaderMenu from '../components/HeaderMenu'
import GoodForCatalog from '../components/GoodForCatalog'
import Footer from '../components/Footer'
import { goods } from '../tempData/tempGoods';

const Catalog = () =>
  <div>
    <HeaderMenu/>
    <hr/>
    <h1>Catalog</h1>
    {
      goods.map(
        ({shortDescription, name, imageSource}) =>
          <GoodForCatalog shortDesc = {shortDescription} name={name} image={imageSource} key={name}/>
      )
    }
    <Footer/>
  </div>

export default Catalog
