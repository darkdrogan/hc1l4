import React from 'react'
import HeaderMenu from '../components/HeaderMenu'
import Contacts from '../components/Contacts'
import Footer from '../components/Footer'

const ContactUs = () =>
  <div>
    <HeaderMenu/>
    <Contacts/>
    <Footer/>
  </div>

export default ContactUs
